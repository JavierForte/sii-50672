
#include <fcntl.h>
#include <iostream>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/stat.h>
#include <unistd.h>


using namespace std; 


int main()	
{
	mkfifo ("/tmp/mififo1", 0777); 
	int fd = open ("/tmp/mififo1", O_RDONLY); 
	char buffer[100]; 
	while (1)	{
		read(fd, buffer, sizeof(buffer)); 	
		if (buffer[0]=='Z'){
			cout<< "Fin del juego"<<endl;	
			break;
		}
		else
			cout<<buffer<<endl; 
	}

	close (fd); 
	unlink("/tmp/mififo1"); 
	return 0; 
}
