#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "DatosMemCompartida.h"
#include "Esfera.h"

int main()	{

	DatosMemCompartida *pDMemC; 
	char *org; 	
	int a = open ("/tmp/bot.txt", O_RDWR); 

	org = static_cast <char *> (mmap(NULL, sizeof(*(pDMemC)), PROT_READ | PROT_WRITE, MAP_SHARED, a, 0)); 
	pDMemC = (DatosMemCompartida *) org; 
	close (a); 

	while (1)	
{
		float centro = (pDMemC->raqueta1.y1 + pDMemC->raqueta1.y2) / 2; 

		if ((centro+(pDMemC->raqueta1.y1-pDMemC->raqueta1.y2)/2) < pDMemC->esfera.centro.y)
			pDMemC->accion = 1; 

		else if ((centro-(pDMemC->raqueta1.y1-pDMemC->raqueta1.y2)/2) > pDMemC->esfera.centro.y)
			pDMemC->accion = -1; 		

		else 
			pDMemC->accion = 0; 

		usleep(25000);
	}

	munmap(org, sizeof(*(pDMemC))); 
	return 0; 
}
