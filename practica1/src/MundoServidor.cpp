// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <signal.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void* hilo_comandos(void* d);

void alarma(int num)
{
	switch (num)	{
		case SIGINT: 
			exit (num); 
		case SIGPIPE:
			exit (num); 
		case SIGUSR1:
			exit (0); 
		case SIGTERM:
			exit (num); 
	} 
}

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	write(tuberia0, "Z", sizeof(char));   
	close (tuberia0); 
	conectar.Close();
	comunicar.Close(); 
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	sprintf(cad, "Servidor"); 
	print(cad,330,0,1,1,1); 
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);

	char buffer[100];

	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;

		sprintf(buffer, "Jugador 2 marca 1 punto, lleva %d puntos en total", puntos2); 
		write (tuberia0, buffer, sizeof(buffer)); 
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;

		sprintf(buffer, "Jugador 1 marca 1 punto, lleva %d puntos en total", puntos1); 
		write (tuberia0, buffer, sizeof(buffer)); 
	}

	if (puntos1 >= 7 && (puntos1-puntos2) >= 2){
		sprintf(buffer, "Gana el jugador1: %d a %d", puntos1, puntos2); 
		write (tuberia0, buffer, sizeof(buffer)); 
		exit (0);
	}
	else if (puntos2 >= 7 && (puntos2-puntos1) >= 2){
		sprintf(buffer,"Gana el jugador2: %d a %d", puntos2, puntos1); 
		write (tuberia0, buffer, sizeof(buffer)); 
		exit (0);
	}

	char cad[200];
	sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x,esfera.centro.y,jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, 	puntos2);
	comunicar.Send(cad, sizeof(cad)); 

	struct sigaction act; 
	act.sa_handler = alarma;
	act.sa_flags = 0; 
	sigaction (SIGINT,  &act, NULL);
	sigaction (SIGTERM, &act, NULL);
	sigaction (SIGPIPE, &act, NULL);
	sigaction (SIGUSR1, &act, NULL);

}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{

}

void CMundo::Init()
{
	Plano p;
	//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

	//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	tuberia0 = open ("/tmp/mififo1", O_WRONLY);


	if( tuberia0==-1)
		perror("Error de apertura");

	pthread_create (&th1, NULL, hilo_comandos, this);

	char ip[] = "127.0.0.1";
	int puerto = 3550;
	conectar.Connect(ip,puerto);
	conectar.InitServer(ip,puerto);
	comunicar = conectar.Accept();
	char nom[100];
	comunicar.Receive(nom, sizeof(nom));
	std::cout << nom << std::endl;
}

void * hilo_comandos (void *d)
{
	CMundo *p=(CMundo *) d;
	p->RecibeComandosJugador();
}

void CMundo::RecibeComandosJugador()
{
	while (1) {
		usleep(10);
		char cad[100];
		comunicar.Receive(cad, sizeof(cad));
		unsigned char key;
		sscanf(cad,"%c",&key);

		switch(key)
		{
			case 's':jugador1.velocidad.y=-4;break;
			case 'w':jugador1.velocidad.y=4;break;
			case 'l':jugador2.velocidad.y=-4;break;
			case 'o':jugador2.velocidad.y=4;break;
		}
	}
}
